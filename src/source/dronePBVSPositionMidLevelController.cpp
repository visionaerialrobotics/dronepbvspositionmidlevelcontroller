/*
 * PBVS_PositionMidLevelController.cpp
 *
 *  Created on: Nov 20, 2012
 *      Author: jespestana
 */

#include "dronePBVSPositionMidLevelController.h"

PBVS_PositionMidLevelController::PBVS_PositionMidLevelController(int idDrone, const std::string &stackPath_in) :
    started(false)
    {
        std::cout << "Constructor: PBVS_PositionMidLevelController" << std::endl;
        try {
        XMLFileReader my_xml_reader( stackPath_in+"config/drone"+cvg_int_to_string(idDrone)+"/pbvs_controller_config.xml");

        // Just in case, I set to zero all the variables of the controller object
        xrel_ci = 0.0; yrel_ci = 0.0; zrel_ci = 0.0; yawrel_ci = 0.0;
        xrel_s = 0.0; yrel_s = 0.0; zrel_s = 0.0; yawrel_s = 0.0;
        eps_x = 0.0; eps_y = 0.0;eps_yaw = 0.0; eps_z = 0.0;
        vxc_int = 0.0; vyc_int = 0.0;
        pitchco = 0.0; rollco = 0.0; dyawco = 0.0; dzco = 0.0;

        double MULTIROTOR_PBVSCONTROLLER_DYAWMAX = my_xml_reader.readDoubleValue( "pbvs_controller_config:output_saturations:MULTIROTOR_PBVSCONTROLLER_DYAWMAX" );
        double MULTIROTOR_PBVSCONTROLLER_DZMAX = my_xml_reader.readDoubleValue( "pbvs_controller_config:output_saturations:MULTIROTOR_PBVSCONTROLLER_DZMAX" );
        MULTIROTOR_PBVSCONTROLLER_MAX_TILT = my_xml_reader.readDoubleValue( "pbvs_controller_config:output_saturations:MULTIROTOR_PBVSCONTROLLER_MAX_TILT" );

        MULTIROTOR_FAERO_DCGAIN_SPEED2TILT = my_xml_reader.readDoubleValue( "pbvs_controller_config:speed_controller:MULTIROTOR_FAERO_DCGAIN_SPEED2TILT" );
        vxy_max = my_xml_reader.readDoubleValue( "pbvs_controller_config:speed_controller:vxy_max" );

        {
        double xy_Kp, xy_Ki, xy_Kd;
        double MULTIROTOR_PBVSCONTROLLER_DX2P_DELTA_KP = my_xml_reader.readDoubleValue( "pbvs_controller_config:position_controller:xy:MULTIROTOR_PBVSCONTROLLER_DX2P_DELTA_KP" );
        double MULTIROTOR_PBVSCONTROLLER_DX2P_KP = my_xml_reader.readDoubleValue( "pbvs_controller_config:position_controller:xy:MULTIROTOR_PBVSCONTROLLER_DX2P_KP" );
        double MULTIROTOR_PBVSCONTROLLER_DX2P_INVTI = my_xml_reader.readDoubleValue( "pbvs_controller_config:position_controller:xy:MULTIROTOR_PBVSCONTROLLER_DX2P_INVTI" );
        double MULTIROTOR_PBVSCONTROLLER_DX2P_TD = my_xml_reader.readDoubleValue( "pbvs_controller_config:position_controller:xy:MULTIROTOR_PBVSCONTROLLER_DX2P_TD" );
        xy_Kp = MULTIROTOR_PBVSCONTROLLER_DX2P_DELTA_KP * MULTIROTOR_PBVSCONTROLLER_DX2P_KP;
        xy_Ki = xy_Kp * MULTIROTOR_PBVSCONTROLLER_DX2P_INVTI;
        xy_Kd = xy_Kp * MULTIROTOR_PBVSCONTROLLER_DX2P_TD;
        // x PID gains: Kp, Ki = Kp/Ti, Kd = Kp*Td
        pid_x.setGains( xy_Kp, xy_Ki, xy_Kd);
        pid_x.enableMaxOutput(true,  vxy_max);
        pid_x.enableAntiWindup(true, vxy_max);
        }
        {
        double xy_Kp, xy_Ki, xy_Kd;
        double MULTIROTOR_PBVSCONTROLLER_DY2R_DELTA_KP = my_xml_reader.readDoubleValue( "pbvs_controller_config:position_controller:xy:MULTIROTOR_PBVSCONTROLLER_DY2R_DELTA_KP" );
        double MULTIROTOR_PBVSCONTROLLER_DY2R_KP = my_xml_reader.readDoubleValue( "pbvs_controller_config:position_controller:xy:MULTIROTOR_PBVSCONTROLLER_DY2R_KP" );
        double MULTIROTOR_PBVSCONTROLLER_DY2R_INVTI = my_xml_reader.readDoubleValue( "pbvs_controller_config:position_controller:xy:MULTIROTOR_PBVSCONTROLLER_DY2R_INVTI" );
        double MULTIROTOR_PBVSCONTROLLER_DY2R_TD = my_xml_reader.readDoubleValue( "pbvs_controller_config:position_controller:xy:MULTIROTOR_PBVSCONTROLLER_DY2R_TD" );
        xy_Kp = MULTIROTOR_PBVSCONTROLLER_DY2R_DELTA_KP * MULTIROTOR_PBVSCONTROLLER_DY2R_KP;
        xy_Ki = xy_Kp * MULTIROTOR_PBVSCONTROLLER_DY2R_INVTI;
        xy_Kd = xy_Kp * MULTIROTOR_PBVSCONTROLLER_DY2R_TD;
        // y PID gains: Kp, Ki = Kp/Ti, Kd = Kp*Td
        pid_y.setGains( xy_Kp, xy_Ki, xy_Kd);
        pid_y.enableMaxOutput( true, vxy_max);
        pid_y.enableAntiWindup(true, vxy_max);
        }
        {
        double yaw_Kp, yaw_Ki, yaw_Kd, yaw_DeltaKp;
        yaw_DeltaKp = my_xml_reader.readDoubleValue( "pbvs_controller_config:position_controller:yaw:DeltaKp" );
        yaw_Kp = yaw_DeltaKp*my_xml_reader.readDoubleValue( "pbvs_controller_config:position_controller:yaw:Kp" );
        yaw_Ki = yaw_DeltaKp*my_xml_reader.readDoubleValue( "pbvs_controller_config:position_controller:yaw:Ki" );
        yaw_Kd = yaw_DeltaKp*my_xml_reader.readDoubleValue( "pbvs_controller_config:position_controller:yaw:Kd" );
        // yaw PID gains: Kp, Ki = Kp/Ti, Kd = Kp*Td
        pid_yaw.setGains( 	yaw_Kp, yaw_Ki, yaw_Kd);
        pid_yaw.enableMaxOutput( true,  MULTIROTOR_PBVSCONTROLLER_DYAWMAX);
        pid_yaw.enableAntiWindup( true, MULTIROTOR_PBVSCONTROLLER_DYAWMAX);
        }
        {
        double z_Kp, z_Ki, z_Kd, z_DeltaKp;
        z_DeltaKp = my_xml_reader.readDoubleValue( "pbvs_controller_config:position_controller:altitude:DeltaKp" );
        z_Kp = z_DeltaKp*my_xml_reader.readDoubleValue( "pbvs_controller_config:position_controller:altitude:Kp" );
        z_Ki = z_DeltaKp*my_xml_reader.readDoubleValue( "pbvs_controller_config:position_controller:altitude:Ki" );
        z_Kd = z_DeltaKp*my_xml_reader.readDoubleValue( "pbvs_controller_config:position_controller:altitude:Kd" );
        // z PID gains: Kp, Ki = Kp/Ti, Kd = Kp*Td
        pid_z.setGains( z_Kp, z_Ki, z_Kd);
        pid_z.enableMaxOutput( true,  MULTIROTOR_PBVSCONTROLLER_DZMAX);
        pid_z.enableAntiWindup( true, MULTIROTOR_PBVSCONTROLLER_DZMAX);
        }

        setControlMode(Controller_MidLevel_controlMode::PBVS_TRACKER_IS_REFERENCE);
    } catch ( cvg_XMLFileReader_exception &e) {
        throw cvg_XMLFileReader_exception(std::string("[cvg_XMLFileReader_exception! caller_function: ") + BOOST_CURRENT_FUNCTION + e.what() + "]\n");
    }
}

PBVS_PositionMidLevelController::~PBVS_PositionMidLevelController() {
}

void PBVS_PositionMidLevelController::setFeedback(double xrel_s_t, double yrel_s_t, double yawrel_s_t, double zrel_s_t) {
    xrel_s   = xrel_s_t;
    yrel_s   = yrel_s_t;
    yawrel_s = yawrel_s_t;
    zrel_s   = zrel_s_t;
}

void PBVS_PositionMidLevelController::setReference( double xrel_ci_t, double yrel_ci_t, double yawrel_ci_t, double zrel_ci_t) {
    xrel_ci   = xrel_ci_t;
    yrel_ci   = yrel_ci_t;
    yawrel_ci = yawrel_ci_t;
    zrel_ci   = zrel_ci_t;
}

#define KEEP_IN_RANGE(a, min, max) if (a < min) a = min; else if (a > max) a = max;

void PBVS_PositionMidLevelController::getOutput(double *pitchco_t, double *rollco_t, double *dyawco_t, double *dzco_t) {

	if (!started) {
		reset();
        *pitchco_t = 0.0;
        *rollco_t  = 0.0;
        *dyawco_t  = 0.0;
        *dzco_t    = 0.0;
		started = true;
		return;
	}


    // Horizontal Position Control loop
    // Calculate vxc for speed controller
    pid_x.setReference(xrel_ci);
    pid_x.setFeedback(xrel_s);
    vxc_int = pid_x.getOutput();
    // Calculate vyc for speed controller
    pid_y.setReference(yrel_ci);
    pid_y.setFeedback(yrel_s);
    vyc_int = pid_y.getOutput();

    // Horizontal Speed Control loop
    saturation_2D( vxc_int, vyc_int, &vxc_int, &vyc_int, vxy_max);
    pitchco = vxc_int * MULTIROTOR_FAERO_DCGAIN_SPEED2TILT;
    rollco  = vyc_int * MULTIROTOR_FAERO_DCGAIN_SPEED2TILT;
    KEEP_IN_RANGE( pitchco, -MULTIROTOR_PBVSCONTROLLER_MAX_TILT, +MULTIROTOR_PBVSCONTROLLER_MAX_TILT)
    KEEP_IN_RANGE( rollco , -MULTIROTOR_PBVSCONTROLLER_MAX_TILT, +MULTIROTOR_PBVSCONTROLLER_MAX_TILT)

    // Compute {dYawdt} output command from {Yawc} input command
    double yaw_error = cvg_utils_library::getAngleError( yawrel_ci, yawrel_s);
    pid_yaw.setReference(yawrel_s+yaw_error); // pid_yaw.setReference(yawci);
    pid_yaw.setFeedback(yawrel_s);
    dyawco = pid_yaw.getOutput();   // Saturation already performed either by PID

    // Compute {Z} output command from {Z} input command
    pid_z.setReference(zrel_ci);
    pid_z.setFeedback(zrel_s);
    dzco = pid_z.getOutput();       // Saturation already performed either by PID

    *pitchco_t = pitchco;
    *rollco_t  = rollco ;
    *dyawco_t  = dyawco ;
    *dzco_t    = dzco   ;
    return;
}

void PBVS_PositionMidLevelController::reset() {

	// Just in case, I set to zero all the variables of the controller object
    xrel_ci = 0.0; yrel_ci = 0.0; zrel_ci = 0.0; yawrel_ci = 0.0;
    xrel_s = 0.0; yrel_s = 0.0; zrel_s = 0.0; yawrel_s = 0.0;
    eps_x = 0.0; eps_y = 0.0;eps_yaw = 0.0; eps_z = 0.0;
    vxc_int = 0.0; vyc_int = 0.0;
    pitchco = 0.0; rollco = 0.0; dyawco = 0.0; dzco = 0.0;

    started = false;

	// Reset internal PIDs
	pid_x.reset();
	pid_y.reset();
    pid_z.reset();
    pid_yaw.reset();

    setControlMode(Controller_MidLevel_controlMode::PBVS_TRACKER_IS_REFERENCE);
}

void PBVS_PositionMidLevelController::saturation_2D(double x1, double x2, double *y1, double *y2, double max) {

    double modulus = sqrt( pow( x1 ,2) + pow( x2 ,2) );

	if ( fabs(modulus) > max ) {
		*y1 = (max/modulus)*x1;
		*y2 = (max/modulus)*x2;
	} else {
		*y1 = x1;
		*y2 = x2;
	}

}

void PBVS_PositionMidLevelController::setControlMode(Controller_MidLevel_controlMode::controlMode mode) {

//	switch (mode) {
//    case Controller_MidLevel_controlMode::PBVS_TRACKER_IS_REFERENCE:
//		break;
//    case Controller_MidLevel_controlMode::PBVS_TRACKER_IS_FEEDBACK:
//		break;
//	}

	control_mode = mode;
}

Controller_MidLevel_controlMode::controlMode PBVS_PositionMidLevelController::getControlMode() {
	return control_mode;
}


void PBVS_PositionMidLevelController::getCurrentPositionReference(double *xrel_ci_t, double *yrel_ci_t, double *zrel_ci_t, double *yawrel_ci_t) {
    (*xrel_ci_t)   = xrel_ci;
    (*yrel_ci_t)   = yrel_ci;
    (*zrel_ci_t)   = zrel_ci;
    (*yawrel_ci_t) = yawrel_ci;
}
